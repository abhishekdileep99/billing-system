#include "date.h"
#ifndef BILL_H
#define BILL_H
#define MAX_BILLS_TT 2000
#define BILLS_OP "bills_op.txt"
#define MAX_TRANSACTION 5
#define MIN_TRANSACTION 1
#include <stdio.h>
    struct item 
    {
        char item_name[100] ; 
        float amt; 
        int qnty;
    };
    typedef struct item item_t;

    struct bill
    {
        int bill_no__ ; 
        int customer_uid__ ; 
        date_t bill_date__ ; 
        float bill_amt__ ;
        int time_;
        int tt_transcations__ ;
        item_t item_list[MAX_TRANSACTION];
    };
    typedef struct bill bill_t;

    

    void __init__bill(bill_t *temp);
    void create_bill(int c_uid , date_t tdate   , bill_t *bill_cus ,  bill_t* db_bills , int time , item_t *item_list , int TT_transcations);
    void fdisplay_bill(const bill_t *bill , FILE *fp);
    
    int  get_bill_no(const bill_t *bill );
    void all_bills_fdisplay( bill_t* db_bills );
    
    int get_tt_bills();
    void insert_transcations(int tt_transcations , item_t *item_list , bill_t *bill_cus );
    void insert_item(item_t *item , char item_name[50] , int qty , int unit_price);
    void display_item(const item_t* item , FILE* fp);
    void display_items(const item_t* item_list , int tt_transcations , FILE* fp );
#endif
