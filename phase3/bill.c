#include "bill.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int __BILL_ID = 0 ;
// bill_t __db_bills[MAX_BILLS_TT];

void create_bill(int c_uid , date_t tdate   , bill_t *bill_cus ,  bill_t* db_bills , int time , item_t *item_list , int TT_transcations)
{   //printf("\n 01");
    bill_cus->bill_no__ = __BILL_ID++;
    bill_cus->customer_uid__ = c_uid ;
    bill_cus->bill_date__ = tdate;
    bill_cus->time_ = time;
    bill_cus->bill_amt__ = 0 ; 
    bill_cus->tt_transcations__ = TT_transcations ;
    //printf(" 02");
    insert_transcations( TT_transcations , item_list , bill_cus );
    //printf(" 03");
    db_bills[bill_cus->bill_no__] = *bill_cus;
    //printf(" 04\n");
}

int  get_bill_no(const bill_t *bill )
{
    return bill->bill_no__;
}

void fdisplay_bill(const bill_t *bill , FILE *fp)
{
    fprintf(fp , "\nCustomer ID : %d Bill Amount : %.2f INR Bill ID : %d Date : " , bill->customer_uid__ , bill->bill_amt__ , bill->bill_no__);
    display_date( &bill->bill_date__ , fp); 
    fprintf(fp , " %4d hrs\n" , bill->time_  );
    display_items(bill->item_list , bill->tt_transcations__ , fp);
}

void all_bills_fdisplay( bill_t* db_bills )
{
    FILE *fp = fopen( BILLS_OP , "w+");

    for(int i = 0 ; i < __BILL_ID ; i++ )
    {
        fdisplay_bill(db_bills + i , fp );
    }
    fclose(fp);
}

void insert_transcations(int tt_transcations , item_t *item_list , bill_t *bill_cus )
{
     
    for(int i = 0 ; i < tt_transcations ; i++ )
    {
        bill_cus->item_list[i].amt = item_list[i].amt;
        strcpy(bill_cus->item_list[i].item_name , item_list[i].item_name);
        bill_cus->item_list[i].qnty = item_list[i].qnty;
        bill_cus->bill_amt__ += item_list[i].amt;
    }
}

void insert_item(item_t *item , char item_name[50] , int qty , int unit_price)
{
    item->amt = qty * unit_price;
    item->qnty = qty; 
    strcpy ( item->item_name , item_name);
}

int get_tt_bills()
{
    return __BILL_ID;
}

void display_item(const item_t* item , FILE* fp)
{
    fprintf(fp , " %15s : QTY : %5d | %8.2f ", item->item_name , item->qnty ,  item->amt   );
}

void display_items(const item_t* item_list , int tt_transcations , FILE* fp )
{
    for(int i = 0 ; i < tt_transcations ; i++ )
    {
        fprintf(fp ,  "%5d->" , i+1 , item_list[i].qnty);
        display_item(item_list + i , fp);
        fprintf(fp , "\n");
    }
}
