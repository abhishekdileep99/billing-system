#ifndef ITEM_MASTER_H
#define ITEM_MASTER_H
#define ITEM_FILE "item_price_list.txt"
#define MAX_ITEMS_LIST 50
struct item_master
{
    int serial_id;
    char name[100];
    float price ; 
};
typedef struct item_master item_master_t;

void insert_item_master( item_master_t *db_item_master  );
void update_item_no();
int get_tt_items();
char* item_name(int item_num , item_master_t *db_bills );
#endif