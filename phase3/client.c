#include <stdio.h>
#include <stdlib.h>
#include "sales.h"
// #include "date.h"
#include "input.h"

int main()
{

    printf("Wazzzup\n");
    // date_t dt;
    printf("\n");
    customer_t* db_customer ;
    db_customer  = (customer_t *)calloc( MAX_CUSTOMER , sizeof(customer_t));
    bill_t *db_bills;
    db_bills = (bill_t * )calloc( MAX_BILLS_TT , sizeof(bill_t) );
    item_master_t *db_item_master;
    db_item_master = (item_master_t * )calloc( MAX_ITEMS_LIST , sizeof(item_master_t));
    
    
    read_input(db_customer , db_bills , db_item_master);
    fdisplay(db_customer , db_bills);
    date_t start_dt ; 
    date_t end_dt;
    input_range_Date(&start_dt , &end_dt);
    FILE *fp = fopen( SALES , "w+" );
    fprintf(fp , "----------------------- BILLS PER CUSTOMER-------------------" );
    fclose(fp);
    fflush(fp);
    tt_sales_per_customer(db_customer);
    fp = fopen( SALES , "a+" );
    fprintf(fp , "----------------------- BILLS PER DAY-------------------" );
    fclose(fp);
    fflush(fp);
    tt_sales_per_day(db_bills , &start_dt , &end_dt );
    fp = fopen( SALES , "a+" );
    fprintf(fp , "----------------------- BILLS PER WEEK DAY-------------------" );
    fclose(fp);
    fflush(fp);
    tt_sales_per_weekday(db_bills , &start_dt , &end_dt );
    fp = fopen( SALES , "a+" );
    fprintf(fp , "----------------------- BILLS PER HOUR -------------------" );
    fclose(fp);
    fflush(fp);
    tt_sales_per_hour(db_bills , &start_dt , &end_dt  );
    fp = fopen( SALES_ITEM , "w+");
    fprintf(fp , "\n----------------------- TOTAL SALES PER ITEM PER DAY  -------------------\n" );
    fclose(fp);
    fflush(fp);
    tt_item_sales_per_day(db_bills ,  start_dt , end_dt , db_item_master  );
    fp = fopen( SALES_ITEM , "a+");
    fprintf(fp , "\n----------------------- TOTAL SALES IN PERIOD TOTAL -------------------\n" );
    fclose(fp);
    fflush(fp);
    item_sales_period(db_bills , db_item_master , &start_dt , &end_dt );
    //printf("%s ", item_name(49 , db_item_master ));
    printf("End");
}
