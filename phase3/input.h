#ifndef INPUT_H
#define INPUT_H
#define INPUT_DATE "billing_dates.txt"
#define NAME_FILE "names.txt"
#include "customer.h"
#include "bill.h"
#include "item_master.h"
int input_Customer(char *names[]);
void input_range_Date(date_t *strtdt , date_t *enddt);
void random_name(char * str , char *names[50] , int nSize) ; 
void read_input( customer_t *db_customer , bill_t *db_bills  , item_master_t* db_item_master );
int __is_unique(char *str , char *names[50] , int nName );
void __gen_day_bill(  char *names[50] , customer_t *cust_file , date_t *startdt , bill_t* db_bills,customer_t* db_customer , int nCustomer ,  item_master_t* db_item_master);
void fdisplay(customer_t *db_customer , bill_t *db_bills  );
int random_time(int *hh , int *min , int inc);
int time_checker( bill_t* db_bills );
void day_check(int n );
void __gen_item_list( item_t *items_list,  int tt_transcation ,  item_master_t* db_item_master  );
int __is_item_unique(int* arr , int nSize , int item_no);
int random_int( int lower , int upper );
#endif


