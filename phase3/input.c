#include "date.h"
#include "input.h"
#include "customer.h"
#include "bill.h"
#include "item_master.h"
#include<stdio.h> 
#include<stdlib.h>
#include <time.h>
#include <string.h>

#define __MAX_CHAR 99
#define __MIN_BILL 20
#define __MAX_BILL 40
#define __MIN_AMT 100
#define __MAX_AMT 5000
#define END_TIME_SHOP 2200
#define BREAK_START 1300
#define BREAK_END 1600
#define START_TIME 800
#define LEAVE_DAY 1
#define SAT 5
#define SUN 6

void read_input( customer_t *db_customer , bill_t *db_bills, item_master_t* db_item_master )
{ 
    date_t strtdt ; 
    date_t enddt;
    time_t t ; 
    char *names[50];
    srand( (unsigned) time(&t) );
    int nCustomer = input_Customer(names);
    input_range_Date( &strtdt , &enddt );
    insert_item_master(db_item_master);
    date_t* nextDT = &strtdt; 
    date_t* end_dt = &enddt;
    end_dt = next_date(end_dt);
    customer_t *cust_file ; 
    int tt_days = calculate_day(&strtdt , end_dt);
    for(int i = 0 ; i < tt_days ; i++  )
    {
        if(nextDT->day != LEAVE_DAY)
        {
            
            __gen_day_bill(  names  , cust_file , nextDT , db_bills , db_customer , nCustomer , db_item_master );
        }        
       nextDT = next_date(nextDT);
        
    }
    time_checker(db_bills);
    // printf("7");
   
}

void __gen_day_bill(   char *names[50] ,  customer_t *cust_file , date_t *startdt ,  bill_t* db_bills,customer_t* db_customer , int nCustomer , item_master_t* db_item_master )
{
    
    int tt_bills_per_day ; 
    int hh = 8;
    int min = 00;
    char str[100];
    if(startdt->day == SUN || startdt->day == SAT)
    {
        tt_bills_per_day = random_int( 50 , 60 );
    }else 
    {
        tt_bills_per_day = random_int( __MIN_BILL , __MAX_BILL);
    }
    
    for(int j = 0 ; j < tt_bills_per_day ; j++ )
    {
            // do
            // {
            //     random_name(str , names);
            // }while( !__is_unique( str , unique_arr , nName) );
            
            // unique_arr[nName] = (char *)malloc(sizeof(char)* 9999);
            // strcpy(unique_arr[nName] , str);
            // nName++;
            //printf("%d " , random_time( &hh , &min ) );
            
            
            //      customer_t *new_cus =  db_customer + __UID; 
            // // bill_t *bill = (bill_t *)malloc(sizeof(bill_t));
            // strcpy ( new_cus->name__ ,  name);
            // new_cus->uid__ = __UID;
            // new_cus->tt_purchase__ = 0;
            // create_bill(__UID , date , amt , bill, db_bills ,  time );
            // new_cus->customer_bill__[new_cus->tt_purchase__] = *bill;
            // __UID++;
            // new_cus->tt_purchase__ += 1;

            
            // bill_t *bill = (bill_t *)malloc(sizeof(bill_t));
            // create_bill(uid->uid__ , date , amt , bill , db_bills , time);
            // uid->customer_bill__[uid->tt_purchase__ ] = *bill;
            // uid->tt_purchase__ +=1;
            //printf("0");
            random_name( str , names , nCustomer);
            int uid = __find_customer_UID(str , db_customer);    
            //printf("1");
            int time = random_time( &hh , &min , ( (11*60) / tt_bills_per_day ) );
            //printf("2");
            int TT_transcation = random_int( MIN_TRANSACTION , MAX_TRANSACTION);
            //printf("3");
            bill_t* bill = (bill_t *)malloc(sizeof(bill_t) );
            item_t item_list[5] ; 
    
            __gen_item_list( item_list ,  TT_transcation , db_item_master  );
            //printf("4");
            //display_items(item_list , TT_transcation , stdout);
            create_bill( uid , *startdt , bill , db_bills , time , item_list , TT_transcation);
            insert_customer( cust_file , str , uid , bill , db_customer );
            // printf("5\n");
    }
     
}

void __gen_item_list( item_t* item_list ,  int tt_transcation , item_master_t* db_item_master )
{
   // printf("\n TT_transctions : %d\n" , tt_transcation);
    int item_no;
    int arr[6];
    int all_items_per_transction =0 ;
    for(int i = 0 ; i < tt_transcation ; i++ )
    {
        int total_items =  get_tt_items();
        while( 1 ) {
            item_no = random_int( 0 , total_items-1 );
            if(__is_item_unique(arr , all_items_per_transction , item_no ))
            {
                arr[all_items_per_transction++] = item_no; 
                break;
            }
        }
        
        
        int qnty = random_int( 1 ,5  );
        insert_item( item_list + i , db_item_master[item_no].name , qnty , db_item_master[item_no].price );
    }
    memset(arr , 0 , 6);
}

int __is_item_unique(int* arr , int nSize , int item_no)
{
    for(int i = 0 ; i <= nSize ; i++ )
    {
        if(arr[i] == item_no)
            return 0;
    }
    return 1;
}
//Change time input if asked 
int random_time(int *hh , int *min , int inc )
{
    if(*min >= 60 )
        {
            *min = 0;
            *hh = *hh+1;
        }
    int rand_time = random_int(0 , inc - 1);
    int time = (*hh)*(100) + *min + rand_time ;
    *min = *min + inc ; 
    
    int min_ptr = time%100;
    if(min_ptr > 59) 
    {
        min_ptr -= inc; 
        min_ptr += 1 ;

    }

    if( time > 2200 )
        {
            perror("Time bill no genearted : shop closed ");
            exit(EXIT_FAILURE);
        }  
    if( BREAK_START <= time && time <= BREAK_END )
        time = random_time(hh , min , inc );
          
        return time ; 
}

int __is_unique(char *str , char *names[50] , int nName )
{  
    for(int i =  0 ; i < nName ; i++ )
    {
        if( strncmp( str , names[i] , 9999) == 0 )
            return 0; 
    }
    return 1; 
}


int time_checker( bill_t* db_bills )
{
     int tt_bills = get_tt_bills();
    for(int i = 0 ; i < tt_bills ; i++ )
    {
        int time = db_bills[i].time_;
        if ( time %100 > 59 )
        {
            db_bills[i].time_ = db_bills[i].time_ + 40 ;
        }
        time = db_bills[i].time_;
        if (time > END_TIME_SHOP )
        {
           db_bills[i].time_ = db_bills[i].time_ -100 ;
        }else if(time >= BREAK_START && time <= BREAK_END ){
            db_bills[i].time_ = db_bills[i].time_ + 500 ; 
        }
    }
}

void input_range_Date(date_t *strtdt , date_t *enddt)
{

    FILE *fp = fopen( INPUT_DATE , "r+");
    char* buffer = (char *)malloc(sizeof(buffer) * __MAX_CHAR); 
    if(fp == NULL )
    {
        perror("file not exisits");
        exit(EXIT_FAILURE);
    }
    int MM;
    int DD;
    int YYYY;
    int day; 
    fscanf( fp , "%d-%d-%d %d\n" , &DD ,&MM ,&YYYY , &day);
    day_check(day);
    insert_date(MM , DD , YYYY , strtdt ,  day );
    fscanf( fp , "%d-%d-%d\n" , &DD ,&MM ,&YYYY);
    insert_date(MM , DD , YYYY , enddt , 0 );
    int diff_day =  getDifference( *strtdt , *enddt );      
    for(int i = 0 ; i < diff_day ; i++ )
    {
         day = (day+1)%7;   
    }
    insert_date(MM , DD , YYYY , enddt , day );
    enddt = next_date(enddt);
    fclose(fp);
    fflush(fp);
}


int input_Customer(char *names[50]) 
{
    FILE *fp = fopen( NAME_FILE , "r");
    char fbuffer[__MAX_CHAR]; 
    char lbuffer[__MAX_CHAR];  
    int i = 0 ; 
    if(fp == NULL )
    {
        perror("file not exisits");
        exit(EXIT_FAILURE);
    }
    while ( fscanf( fp , "%s %s" , fbuffer , lbuffer ) != EOF )
    {
        names[i] = (char *)malloc(sizeof(char)*100);
        strcpy( names[i] , fbuffer );  
        i++;  
    }
    fclose(fp);
    return i-1;
}

void random_name(char *str , char *names[50] , int nSize)
{   
    strcpy(str , names[ random_int(0 , nSize ) ] );
}


int random_int( int lower , int upper )
{
    return  ( lower +  (rand()% (upper - lower + 1 ))  ) ; 
}


void fdisplay(customer_t *db_customer , bill_t *db_bills  )
{
    all_customers_fdisplay(db_customer);
    all_bills_fdisplay(db_bills);
}

void day_check(int n )
{
    if( n >6 || n <0 )
    {
        perror("Error date ");
        exit(EXIT_FAILURE);
    }
}
