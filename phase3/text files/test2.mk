# op.exe : test_client.o input.o bill.o date.o customer.o sales.o
# 	gcc date.o bill.o input.o customer.o test_client.o sales.o -o op.exe
# sales.o : sales.c sales.h customer.h bill.h date.h
# 	gcc -c sales.c
# test_client.o : input.c input.h customer.h bill.h date.h test_client.c
# 	gcc -c test_client.c
# input.o : input.c bill.h customer.h date.h
# 	gcc -c input.c
# customer.o : customer.c customer.h
# 	gcc -c customer.c
# bill.o : bill.c bill.h date.h
# 	gcc -c bill.c
op.exe : test_client.o date.o
	gcc test_client.o date.o -o op.exe
test_client.o : test_client.c date.h date.c 
	gcc -c test_client.c
date.o : date.c date.h
	gcc -c date.c