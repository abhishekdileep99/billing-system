#ifndef CUSTOMER_H
#define CUSTOMER_H
#define MAX_NAME_SIZE 100
#define MAX_CUSTOMER 51
#define CUSTOMER_FILE "customer.txt"
#define CUSTOMER_FILE_OP "customer_op.txt"
#include <stdio.h>
#include "date.h"
#include "bill.h"
#define MAX_BILLS_PERSON 70
struct customer 
{
  int uid__ ; //4
  int tt_purchase__; //4
  char name__ [MAX_NAME_SIZE]; //100 
  bill_t customer_bill__[MAX_BILLS_PERSON];

};
typedef struct customer customer_t;

int __find_customer_UID ( char* name , customer_t* db_customer);
void insert_customer( customer_t* customer , char* name , int uid , bill_t* bill , customer_t *db ); 
void fdisplay_customer(customer_t* customer , FILE *fp);
void all_customers_fdisplay( customer_t* db_customer );
int get_tt_customers();
customer_t* __is_unique_customer ( int uid , customer_t* db_customer );



#endif