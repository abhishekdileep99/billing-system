#ifndef DATE_H
#define DATE_H
#include <stdio.h>
struct date {
    int MM ; 
    int DD;
    int YYYY;
    int day;
};
typedef struct date date_t;
void insert_date(int mm , int dd , int yyyy , date_t * date , int day );
void display_date(const date_t* date , FILE *fp);
int check_leap_year(int yyyy);
int calculate_day(const date_t* lhs , const date_t* rhs);
int countLeapYears(date_t d);
int getDifference(date_t dt1, date_t dt2);
date_t* next_date( date_t* prev );
char* get_day(int day );
//get function to get the charcter of the day 
//
#endif