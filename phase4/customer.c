#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "customer.h"
#include "bill.h" 
#include "date.h"
#include <sys/stat.h>
#define MAX_LINE_SIZE 400

// customer_t __db_customer[MAX_CUSTOMER];
int __UID = 0 ; 

int __find_customer_UID ( char* name , customer_t* db_customer )
{
   for(int i = 0 ; i <= __UID ; i++ )
   {
       if(strcmp (name , db_customer[i].name__ ) == 0)
        {
            return  i ;
        }
   }
    return ++__UID;
//    return __UID;
}

customer_t* __is_unique_customer ( int uid , customer_t* db_customer )
{
   for(int i = 0 ; i < __UID ; i++ )
   {
       if( uid == db_customer[i].uid__ )
            return  db_customer + i  ;
   }
   return NULL;
}



void fdisplay_customer(customer_t* customer , FILE *fp)
{   
    fprintf( fp ,  "%d -> %s\n" , customer->uid__ , customer->name__);
}



void insert_customer( customer_t* customer , char* name , int uid , bill_t* bill , customer_t *db_cusotmer )
{ 
    if(strcmp(name , "" ) == 0 )
    {
        perror("String name Empty");
        exit(EXIT_FAILURE);
    }
    customer_t *pntr = __is_unique_customer(uid , db_cusotmer);  
    if( pntr  == NULL )
    {
        customer_t *new_cus = db_cusotmer + __UID;
        new_cus->uid__ = uid;
        new_cus->customer_bill__[new_cus->tt_purchase__] = *bill;
        new_cus->tt_purchase__ = new_cus->tt_purchase__ + 1;
        strcpy( new_cus->name__ , name  );
        db_cusotmer[uid] = *new_cus;
    }else {
        pntr->customer_bill__[pntr->tt_purchase__++] = *bill;
    }
   
}


void all_customers_fdisplay(customer_t* db_customer)
{
    FILE *fp = fopen(CUSTOMER_FILE_OP , "w+");

    for(int i = 1 ; i< __UID ; i++ )
    {
        fprintf(fp , "\n------------------------\n");
        fdisplay_customer(db_customer + i , fp );
        int j = 0 ;
        while ( j < db_customer[i].tt_purchase__ )
        {
            fdisplay_bill( db_customer[i].customer_bill__ + j , fp  ); 
            j++;
        }
        
        fprintf(fp , "\n------------------------\n");
    }
    fclose(fp);

    //For testing remove these 
    //fprintf(fp , "\n");
    //fdisplay_bill( db_customer[i].customer_bill__ + j , fp  ); 
    //fprintf(fp , "\n");
        
        // fprintf(fp , "\n------------------------\n");
}

int get_tt_customers()
{
    return __UID; 
}