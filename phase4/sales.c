#include<stdio.h>
#include<stdlib.h>
#include "sales.h"
#include <string.h>

void tt_sales_per_customer( customer_t *db_customer  )
{
    int tt_customer = get_tt_customers();
    for(int i = 1 ; i < tt_customer ; i++ )
    {
        int tt_purchase_customer = db_customer[i].tt_purchase__ ;
        double amt = 0 ;  
        for(int j = 0 ; j < tt_purchase_customer ; j++ )
        {
            amt = amt +  db_customer[i].customer_bill__[j].bill_amt__;
        }
        fdisplay_tt_customer(db_customer[i].name__ , amt);
    } 
}

void tt_sales_per_day(  bill_t *db_bills ,date_t *start_dt , date_t* end_dt )
{
    int tt_bills = get_tt_bills(db_bills); 
    int nUnique = 0 ; 
     
    date_t *next_dt = start_dt ;
    end_dt = next_date(end_dt);
    while( !compare(next_dt , end_dt ) )
    {
        

        double amt = 0 ;
        for(int j = 0 ; j < tt_bills ; j++ )
        {
             if( compare( &(db_bills[j].bill_date__) , next_dt ) ) 
             {
               amt = amt + db_bills[j].bill_amt__;  
             }
        }
        fdisplay_tt_bill( *next_dt , amt );
        next_dt = next_date(next_dt); 
    }

}

int find_name_id( char* name[50] , int nSize , char item[100])
{
    for(int i = 0 ; i < nSize ; i++ )
    {
        if( strcmp( name[i] , item ) == 0 )
        {
            return i;
        }
    }
    perror("item doesn't exisit");
    exit(EXIT_FAILURE);
}

void tt_item_sales_per_day( bill_t * db_bills , date_t dt_start , date_t dt_end , item_master_t* db_items )
{
    FILE *fp = fopen(SALES_ITEM , "a+");

    date_t *start_dt = &dt_start;
    date_t *end_dt = &dt_end;
    end_dt = next_date(end_dt);

    

    while( !compare(start_dt , end_dt) )
    {
        fprintf( fp , "\n");
        display_date(start_dt , fp);
        fprintf( fp , "\n\n");
        int tt_items = get_tt_items();
        char* name[50];
        for(int i = 0 ; i < tt_items ; i++ )
        {
            name[i] =  (char *)calloc( 100 ,  sizeof(char)  );
            strcpy(name[i] , db_items[i].name );
        //printf("%s\n" , name[i]);
        }
        float* item_amt = (float *)calloc( 50 , sizeof(float) );
        int tt_bills  = get_tt_bills();
        
        for(int i = 0 ; i < tt_bills ; i++ )
        {   
        //int tt_transcations_per_bill = db_bills[i].tt_transcations__;
        //display_items(db_bills[i].item_list , db_bills[i].tt_transcations__ , stdout);

            for(int j = 0 ; j < db_bills[i].tt_transcations__ ; j++ )
            {
                if( compare( &db_bills[i].bill_date__ , start_dt ) )
                {
                    int item_id = find_name_id( name , 50 , db_bills[i].item_list[j].item_name );
                    item_amt[item_id] += db_bills[i].item_list[j].amt; 
                }
            }
        }
            for(int i = 0 ; i < 50 ; i++ )
            {
                fprintf( fp , "%20s -> Amt : %10.2f\n" , name[i] , item_amt[i]);
            }
        start_dt = next_date(start_dt);
    }

    fclose(fp);
}

void item_sales_period( bill_t * db_bills , item_master_t* db_items , date_t* dt_start , date_t* dt_end )
{
    
    FILE *fw = fopen(INPUT_DATE_RANGE , "r+"  );
    FILE* fp = fopen(SALES_ITEM , "a+");
    date_t* start_dt = (date_t *)malloc(sizeof(date_t) );
    date_t* end_dt = (date_t *)malloc(sizeof(date_t) );
    int tt_items = get_tt_items();
    float* item_amt = (float *)calloc( 50 , sizeof(float) );
    char* name[50];
    int day = 0 ; 
    int DD;
    int MM;
    int YY;
    fscanf( fw , "%d-%d-%d" , &DD , &MM , &YY );
    insert_date(MM ,DD , YY , start_dt , day);
    fscanf( fw , "%d-%d-%d" , &DD , &MM , &YY , &day );
    insert_date(MM ,DD , YY , end_dt , day);
    printf("Checking Period Dates : \n");
    int tt = calculate_day(start_dt , end_dt);
    tt = calculate_day( dt_start , dt_end);
    tt = calculate_day( end_dt , dt_end );
    printf("Period Dates : Pass \n");
    
    end_dt = next_date(end_dt);
    
    for(int i = 0 ; i < tt_items ; i++ )
    {
        name[i] =  (char *)calloc( 100 ,  sizeof(char)  );
        strcpy(name[i] , db_items[i].name );
    }

    while( !compare(start_dt , end_dt) )
    {
        int tt_bills  = get_tt_bills();
        
        for(int i = 0 ; i < tt_bills ; i++ )
        {   
        //int tt_transcations_per_bill = db_bills[i].tt_transcations__;
        //display_items(db_bills[i].item_list , db_bills[i].tt_transcations__ , stdout);

            for(int j = 0 ; j < db_bills[i].tt_transcations__ ; j++ )
            {
                if( compare( &db_bills[i].bill_date__ , start_dt ) )
                {
                    int item_id = find_name_id( name , 50 , db_bills[i].item_list[j].item_name );
                    item_amt[item_id] += db_bills[i].item_list[j].amt; 
                }
            }
        }    
        start_dt = next_date(start_dt);
    }
    for(int i = 0 ; i < 50 ; i++ )
    {
        fprintf( fp , "%20s -> Amt : %10.2f\n" , name[i] , item_amt[i]);
    }

    fclose(fp);
    fclose(fw);
}


void tt_sales_per_weekday(  bill_t *db_bills ,date_t *start_dt , date_t* end_dt )
{
    int tt_bills = get_tt_bills(db_bills); 
    int nUnique = 0 ; 
    double arr[7] = { 0 , 0 , 0  ,0 , 0 , 0 , 0 };
    date_t *next_dt = start_dt ;
    end_dt = next_date(end_dt);
    while( !compare(next_dt , end_dt ) )
    {
        

        double amt = 0 ;
        for(int j = 0 ; j < tt_bills ; j++ )
        {
             if( compare( &(db_bills[j].bill_date__) , next_dt ) ) 
             {
               amt = amt + db_bills[j].bill_amt__;  
             }
        }
        arr[next_dt->day] = arr[next_dt->day] + amt;
        next_dt = next_date(next_dt); 
    }
    next_dt->day = 0;
    next_dt->DD = 0 ; 
    next_dt->YYYY = 0 ; 
    next_dt->MM = 0 ; 
    for(int i = 0 ; i < 7 ; i++ )
    {
        fdisplay_char( get_day(i) , arr[i] );
    }


}

void tt_sales_per_hour(  bill_t *db_bills ,date_t *start_dt , date_t* end_dt )
{
    int tt_bills = get_tt_bills(db_bills); 
    double arr[13] = { 0 , 0 , 0  ,0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0  };
    double amt = 0 ;
    end_dt = next_date(end_dt);
    for(int j = 0 ; j < tt_bills ; j++ )
    {
         int time = db_bills[j].time_;
         amt = db_bills[j].bill_amt__ ;
        //  printf("%d %f \n " , time , amt );
         if(time >= 800 && time < 900 )
         {
             arr[0] +=  amt;
         } else if(time >= 900 && time < 1000 )
         {
             arr[1] += amt;
         } else if(time >= 1000 && time < 1100 )
         {
             arr[2] += amt;
         } else if(time >= 1100 && time < 1200 )
         {
             arr[3] += amt;
         }  else if(time >= 1200 && time < 1300 )
         {
             arr[4] += amt;
         }   
         else if(time >= 1600 && time < 1700 )
         {
             arr[5] += amt;
         }  
         else if(time >= 1700 && time < 1800 )
         {
             arr[6] += amt;
         } else if(time >= 1800 && time < 1900 )
         {
             arr[7] += amt;
         } else if(time >= 1900 && time < 2000 )
         {
             arr[8] += amt;
         } else if(time >= 2000 && time < 2100 )
         {
             arr[9] += amt;
         } else if(time >= 2100 && time < 2200 )
         {
             arr[10] += amt;
         } 
    
    }
    char time[12][100] = { "8AM-9AM" , "9AM-10AM" , "10AM-11AM" , "11AM-12AM" , "12AM-1PM" , "4PM-5PM" , "5PM-6PM" , "6PM-7PM" , "7PM-8PM" , "8PM-9PM" , "9PM-10PM" };    
    for(int i = 0 ; i < 11 ; i++ )
    {
        fdisplay_char_fp(time[i] , arr[i] , "report.txt" );
    }


}

int unique_date( date_t* all_date , date_t date , int nSize  )
{
    for(int i = 0 ; i < nSize ; i++ )
    {
        if( compare(&date , all_date + i  ))
            {
                return 1;
            }
    }
    return 0;
}
  
int compare( const date_t* lhs , const date_t* rhs  )
{
    if((lhs->YYYY == rhs->YYYY )&& (lhs->MM == rhs->MM) && (lhs->DD == rhs->DD ))
    {
        return 1;
    }
    else {
        return 0 ; 
    }
}

void fdisplay_tt_customer(char* name  , double amt)
{
    FILE *fp = fopen( SALES , "a+" );
    fprintf(fp , "\n-> %s : %.2f \n" ,  name , amt  );
    fclose(fp);
}

void fdisplay_tt_bill(date_t Date  , double amt)
{
    FILE *fp = fopen( SALES , "a+" );
    fprintf(fp , "\n-> %d-%d-%d : %.2lf \n" ,  Date.DD , Date.MM , Date.YYYY , amt  );
    fclose(fp);
}

void fdisplay_char( char* str , double amt )
{
    FILE *fp = fopen( SALES , "a+" );
    fprintf(fp , "\n-> %s : %.2lf \n" , str , amt  );
    fclose(fp);
}

void fdisplay_char_fp( char* str , double amt , char* file_name )
{
    FILE *fp = fopen( file_name , "a+" );
    fprintf(fp , "\n-> %s : %.2lf \n" , str , amt  );
    fclose(fp);
}