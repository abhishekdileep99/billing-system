op.exe : client.o input.o bill.o date.o customer.o sales.o item_master.o
	gcc date.o bill.o input.o customer.o client.o sales.o item_master.o -o op.exe
sales.o : sales.c sales.h customer.h bill.h date.h
	gcc -c sales.c
client.o : input.c input.h customer.h bill.h date.h client.c item_master.h item_master.c sales.h sales.c
	gcc -c client.c
input.o : input.c bill.h customer.h date.h item_master.c item_master.h  
	gcc -c input.c
customer.o : customer.c customer.h bill.c bill.h
	gcc -c customer.c
bill.o : bill.c bill.h date.h
	gcc -c bill.c
date.o : date.c date.h
	gcc -c date.c
item_master.o : item_master.c item_master.h
	gcc -c item_master.c