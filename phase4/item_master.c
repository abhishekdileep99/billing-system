#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include "item_master.h"
int __ITEM_NO = 0 ;
void insert_item_master( item_master_t *db_item_master  )
{
    FILE* fp = fopen( ITEM_FILE , "r" );
    char name[100];
    int amt_;
    int serial_id;
    int qty ; 
    while(fscanf(fp , "%d %s %d %d", &serial_id , name , &amt_ , &qty  )!= EOF)
    {
        if(amt_ < 0 )
            {
                perror("what man , i will get money to buy this ? ");
                exit(EXIT_FAILURE);    
            }
        item_master_t * item = (item_master_t *)malloc(sizeof(item_master_t));
        strcpy(item->name , name );
        item->price = amt_;
        item->qnty = qty;
        item->serial_id = get_tt_items();
        db_item_master[get_tt_items()] = *item;
        update_item_no();
    //  printf("%d %s %d\n" , serial_id , name , amt_);
    }
    fclose(fp);
}
void tt_unsold_items( item_master_t * db_item_master  )
{
    FILE* fp = fopen(UNSOLD_ITEMS , "w+");
    int tt_items = get_tt_items();
    for(int i = 0 ; i < tt_items ; i++ )
    {
        if(db_item_master[i].qnty != 0 )
        {
            fprintf( fp ,  " ITEM_NAME : %25s PRICE : %8.2f : QTY : %5d\n" , db_item_master[i].name , db_item_master[i].price , db_item_master[i].qnty );
        }
    }
    fclose(fp);
}

void update_item_no()
{
    __ITEM_NO++;
}
int get_tt_items()
{
    return __ITEM_NO;
}

char* item_name(int item_num , item_master_t *db_bills )
{
    return db_bills[item_num].name;
}