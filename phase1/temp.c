#include<stdio.h>

const int monthDays[12]
    = { 31, 28, 31, 30, 31, 30,
       31, 31, 30, 31, 30, 31 };

struct  date1
{
    int DD;
    int YYYY;
    int MM;
};
typedef struct date1 date_t;

int countLeapYears(date_t d)
{
    int years = d.YYYY;
    if (d.MM <= 2)
        years--;
    return years / 4 - years / 100 + years / 400;
}
int check_leap_year(int yyyy)
{
    int LEAP_YEAR = 0 ; 
    if( ( yyyy%4 ==0  && yyyy%100 != 0 ) || ( yyyy%400 == 0 ) )
    {
        LEAP_YEAR = 1;
    }
    else 
    { 
        LEAP_YEAR = 0 ; 
    }
    return LEAP_YEAR ; 

}

int getDifference(date_t dt1, date_t dt2)
{
   
    long int n1 = dt1.YYYY * 365 + dt1.DD;
 
    for (int i = 0; i < dt1.MM - 1; i++)
        n1 += monthDays[i];
 
    n1 += countLeapYears(dt1);
 
    long int n2 = dt2.YYYY * 365 + dt2.DD;
    for (int i = 0; i < dt2.MM - 1; i++)
        n2 += monthDays[i];
    n2 += countLeapYears(dt2);
 
    return (n2 - n1 + 1);
}



int main()
{
    date_t d;
    d.YYYY = 2020;
    d.MM = 2;
    d.DD = 1 ; 
    date_t d1;
    d1.YYYY = 2020;
    d1.MM = 3;
    d1.DD = 1 ; 
    printf("%d" ,getDifference(d , d1 ));

}