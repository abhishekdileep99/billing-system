#ifndef INPUT_H
#define INPUT_H
#define INPUT_DATE "billing_dates.txt"
#define NAME_FILE "names.txt"
#include "customer.h"
#include "bill.h"
void input_Customer(char *names[]);
void input_range_Date(date_t *strtdt , date_t *enddt);
void random_name(char * str , char *names[50] ) ; 
int random_int(  );
void read_input( customer_t *db_customer , bill_t *db_bills );
int __is_unique(char *str , char *names[50] , int nName );
void __gen_day_bill(  char *str , char *names[50] , customer_t *cust_file , date_t *startdt , bill_t* db_bills,customer_t* db_customer);
void fdisplay(customer_t *db_customer , bill_t *db_bills  );

#endif