#include <stdio.h>
#include <stdlib.h>
#include "input.h"
#include "sales.h"

int main()
{
    customer_t db_customer[MAX_CUSTOMER] ;
    bill_t db_bills[MAX_BILLS_TT];
    read_input(db_customer , db_bills);
    fdisplay(db_customer , db_bills);
    date_t start_dt ; 
    date_t end_dt;
    input_range_Date(&start_dt , &end_dt);
    FILE *fp = fopen( SALES , "w+" );
    fprintf(fp , "----------------------- BILLS PER CUSTOMER-------------------" );
    fclose(fp);
    fflush(fp);
    tt_sales_per_customer(db_customer);
    fp = fopen( SALES , "a+" );
    fprintf(fp , "----------------------- BILLS PER DAY-------------------" );
    fclose(fp);
    fflush(fp);
    tt_sales_per_day(db_bills , &start_dt , &end_dt );
}
