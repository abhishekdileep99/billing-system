#include<stdio.h>
#include<stdlib.h>
#include "sales.h"

void tt_sales_per_customer( customer_t *db_customer  )
{
    int tt_customer = get_tt_customers();
    for(int i = 0 ; i < tt_customer ; i++ )
    {
        int tt_purchase_customer = db_customer[i].tt_purchase__ ;
        double amt = 0 ;  
        for(int j = 0 ; j < tt_purchase_customer ; j++ )
        {
            amt = amt +  db_customer[i].customer_bill__[j].bill_amt__;
        }
        fdisplay_tt_customer(db_customer[i].name__ , amt);
    } 
}
void tt_sales_per_day(  bill_t *db_bills ,date_t *start_dt , date_t* end_dt )
{
    int tt_bills = get_tt_bills(db_bills); 
    int nUnique = 0 ; 
     
    date_t *next_dt = start_dt ;
    end_dt = next_date(end_dt);

    while( !compare(next_dt , end_dt ) )
    {
        

        double amt = 0 ;
        for(int j = 0 ; j < tt_bills ; j++ )
        {
             if( compare( &(db_bills[j].bill_date__) , next_dt ) ) 
             {
               amt = amt + db_bills[j].bill_amt__;  
             }
        }
        fdisplay_tt_bill( *next_dt , amt );
        next_dt = next_date(next_dt); 
    }
}

int unique_date( date_t* all_date , date_t date , int nSize  )
{
    for(int i = 0 ; i < nSize ; i++ )
    {
        if( compare(&date , all_date + i  ))
            {
                return 1;
            }
    }
    return 0;
}
  


int compare( const date_t* lhs , const date_t* rhs  )
{
    if((lhs->YYYY == rhs->YYYY )&& (lhs->MM == rhs->MM) && (lhs->DD == rhs->DD ))
    {
        return 1;
    }
    else {
        return 0 ; 
    }
}

void fdisplay_tt_customer(char* name  , double amt)
{
    FILE *fp = fopen( SALES , "a+" );
    fprintf(fp , "\n-> %s : %.2f \n" ,  name , amt  );
    fclose(fp);
}

void fdisplay_tt_bill(date_t Date  , double amt)
{
    FILE *fp = fopen( SALES , "a+" );
    fprintf(fp , "\n-> %d-%d-%d : %.2f \n" ,  Date.DD , Date.MM , Date.YYYY , amt  );
    fclose(fp);
}