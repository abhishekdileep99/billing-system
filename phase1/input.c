#include "date.h"
#include "input.h"
#include "customer.h"
#include "bill.h"
#include<stdio.h> 
#include<windows.h>
#include<stdlib.h>
#include <time.h>
#include <string.h>

#define __MAX_CHAR 99
#define __MIN_BILL 20
#define __MAX_BILL 40
#define __MIN_AMT 100
#define __MAX_AMT 5000


void read_input( customer_t *db_customer , bill_t *db_bills )
{
    
    date_t strtdt ; 
    date_t enddt;
    time_t t ; 
    char *names[50];
    char str[100];


    srand( (unsigned) time(&t) );
    input_Customer(names);
    input_range_Date( &strtdt , &enddt );
    // printf("---------------- Start date & End Date -----------\n"); //test code 
    // display_date( &strtdt ,stdout); //test code 
    // display_date( &enddt ,stdout); //test code 
    
    date_t* startdt = &strtdt; 
    date_t* end_dt = &enddt;
    end_dt = next_date(end_dt);
    // display_date( &enddt ,stdout); //Test code 
    // printf("\n----------------difference of dates -----------\n"); //test code 
    customer_t *cust_file ; 
    int tt_days = calculate_day(&strtdt , end_dt);
    // printf("%d ", tt_days);  //test code 
    // printf("\n---------------- All Dates for which the loop is running -----------\n"); //test code 
    for( int i = 0 ; i < tt_days ; i++  )
    {
        __gen_day_bill(   str , names  , cust_file , startdt , db_bills , db_customer);
        // display_date( startdt ,stdout); //Test code 
        startdt = next_date(startdt);
    }
    // printf("\n----------------End of input read files  -----------\n"); //test code 
}

void __gen_day_bill(   char *str , char *names[50] ,  customer_t *cust_file , date_t *startdt ,  bill_t* db_bills,customer_t* db_customer)
{
    int tt_bills_per_day = random_int( __MIN_BILL , __MAX_BILL);
    char *unique_arr[50];
    int nName = 0 ;

    for(int j = 0 ; j < tt_bills_per_day ; j++ )
        {
            do
            {
                random_name(str , names);
            }while( !__is_unique( str , unique_arr , nName) );
            
            unique_arr[nName] = (char *)malloc(sizeof(char)* 9999);
            strcpy(unique_arr[nName] , str);
            nName++;

            int amt = random_int(__MIN_AMT , __MAX_AMT );
            insert_customer( cust_file , str , amt  , *startdt , db_bills , db_customer );
        }
}

int __is_unique(char *str , char *names[50] , int nName )
{  
    for(int i =  0 ; i < nName ; i++ )
    {
        if( strncmp( str , names[i] , 9999) == 0 )
            return 0; 
    }
    return 1; 
}

void input_range_Date(date_t *strtdt , date_t *enddt)
{

    FILE *fp = fopen( INPUT_DATE , "r+");
    char* buffer = (char *)malloc(sizeof(buffer) * __MAX_CHAR); 
    if(fp == NULL )
    {
        perror("file not exisits");
        exit(EXIT_FAILURE);
    }
    int MM;
    int DD;
    int YYYY;
    fscanf( fp , "%d-%d-%d\n" , &DD ,&MM ,&YYYY);
    insert_date(MM , DD , YYYY , strtdt );
    fscanf( fp , "%d-%d-%d\n" , &DD ,&MM ,&YYYY);
    insert_date(MM , DD , YYYY , enddt );

    fclose(fp);
    fflush(fp);
}


void input_Customer(char *names[50]) 
{
    FILE *fp = fopen( NAME_FILE , "r");
    char fbuffer[__MAX_CHAR]; 
    char lbuffer[__MAX_CHAR];  
    int i = 0 ; 
    if(fp == NULL )
    {
        perror("file not exisits");
        exit(EXIT_FAILURE);
    }
    while ( fscanf( fp , "%s %s" , fbuffer , lbuffer ) != EOF )
    {
        names[i] = (char *)malloc(sizeof(char)*100);
        strcpy( names[i] , fbuffer );  
        i++;  
    }
    fclose(fp);
}

void random_name(char *str , char *names[50] )
{   
    strcpy(str , names[ rand() % 50 ] );
}


int random_int( int lower , int upper )
{
    return  ( lower +  (rand()% (upper - lower + 1 ))  ) ; 
}


void fdisplay(customer_t *db_customer , bill_t *db_bills  )
{
    all_customers_fdisplay(db_customer);
    all_bills_fdisplay(db_bills);
}
