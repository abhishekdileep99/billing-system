#include "date.h"
#ifndef BILL_H
#define BILL_H
#define MAX_BILLS_TT 9999
#define BILLS_OP "bills_op.txt"
#include <stdio.h>
    struct bill
    {
        int bill_no__ ; 
        int customer_uid__ ; 
        date_t bill_date__ ; 
        float bill_amt__ ;
        int time_;
    };
    typedef struct bill bill_t;

    void __init__bill(bill_t *temp);
    void create_bill(int customer_uid , date_t tdate , float bill_amt  , bill_t *bill , bill_t* db_bills ,int time  );
    void fdisplay_bill(const bill_t *bill , FILE *fp);
    
    int  get_bill_no(const bill_t *bill );
    void all_bills_fdisplay( bill_t* db_bills );
    
    int get_tt_bills();

#endif
