#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>
#include "date.h"

const int monthDays[12]
    = { 31, 28, 31, 30, 31, 30,
       31, 31, 30, 31, 30, 31 };

const int monthDaysLeap[13]
    = { 0 ,   31, 29, 31, 30, 31, 30,
       31, 31, 30, 31, 30, 31 };



void insert_date (int mm , int dd , int yyyy , date_t * date , int day  )
{
    //Helper function to insert dates 
    //Input : Month, day , yearr 
    int LEAP_YEAR = 0 ; 
    int day_of_month[] = { 31 , 28 + check_leap_year(yyyy)  , 31 , 30 , 31 , 30 , 31 , 31 , 30 , 31 ,30 ,31   }; 
    
    if(mm > 12 || mm < 1 )
    {
        perror("invalid month ") ;
        exit(EXIT_FAILURE);  
    }
    else if( dd > day_of_month[mm-1] || dd < 1){
    {
        perror("invalid day ") ;
        exit(EXIT_FAILURE);  
    } 
    }else {
        date->DD = dd;
        date->MM = mm;
        date->YYYY = yyyy;
        date->day = day ;
    }

}




void display_date(const date_t* date , FILE *fp )
{
    fprintf(fp , " %d-%d-%d %s" , date->DD , date->MM , date->YYYY , get_day(date->day) ) ;
    
}





int calculate_day(const date_t *lhs , const date_t *rhs)
{
    int lYY = lhs->YYYY;
    int lMM = lhs->MM;
    int lDD = lhs->DD;

    int rYY = rhs->YYYY;
    int rMM = rhs->MM;
    int rDD = rhs->DD;

    if(lYY > rYY )
    {
        perror("start date > end Date ");
        exit(EXIT_FAILURE);
    }
    else if( ( lMM > rMM ) && (lYY==rYY) ) 
    {
        perror("start date > end Date ");
        exit(EXIT_FAILURE);
    }
    else if ( (lDD > rDD) && ( lMM == rMM ) && ( lYY == rYY ) )
    {
        perror("start date > end Date ");
        exit(EXIT_FAILURE);
    }
     
    int tt_days  = getDifference(*lhs , *rhs );

    return tt_days;
}



int check_leap_year(int yyyy)
{
    int LEAP_YEAR = 0 ; 
    if( ( yyyy%4 ==0  && yyyy%100 != 0 ) || ( yyyy%400 == 0 ) )
    {
        LEAP_YEAR = 1;
    }
    else 
    { 
        LEAP_YEAR = 0 ; 
    }
    return LEAP_YEAR ; 

}




int countLeapYears(date_t d)
{
    int years = d.YYYY;
    if (d.MM <= 2)
        years--;
    return years / 4 - years / 100 + years / 400;
}


int getDifference(date_t dt1, date_t dt2)
{
   
    long int n1 = dt1.YYYY * 365 + dt1.DD;
 
    for (int i = 0; i < dt1.MM - 1; i++)
        n1 += monthDays[i];
 
    n1 += countLeapYears(dt1);
 
    long int n2 = dt2.YYYY * 365 + dt2.DD;
    for (int i = 0; i < dt2.MM - 1; i++)
        n2 += monthDays[i];
    n2 += countLeapYears(dt2);
 
    return (n2 - n1);
}


date_t* next_date( date_t*  prev )
{
    date_t* next_date = (date_t *)malloc(sizeof(date_t));
    int is_leap = check_leap_year(prev->YYYY);
    int day = prev->day ; 
    day = (day+1)%7;
    if(is_leap )
    {
         if(prev->DD == 29 && prev->MM == 2)
        {
            next_date->MM =  3;
            next_date->DD = 1;
            next_date->YYYY = prev->YYYY;
            next_date->day = day;
        }else if( ( prev->DD == monthDaysLeap[prev->MM] ) && (prev->MM == 12)  )
        {
            next_date->YYYY = prev->YYYY + 1;
            next_date->MM = 1;
            next_date->DD = 1;
            next_date->day = day; 
        }else if ( prev->DD == monthDaysLeap[prev->MM] )
        {
            next_date->DD = 1;
            next_date->MM = prev->MM + 1 ;
            next_date->YYYY = prev->YYYY;
            next_date->day = day;
        }else
        {
            next_date->DD = prev->DD +1 ; 
            next_date->MM = prev->MM;
            next_date->YYYY = prev->YYYY;
            next_date->day = day;
        }
        
    }else 
    {
        if(prev->DD == 28 && prev->MM == 2)
        {
            next_date->MM =  3;
            next_date->DD = 1;
            next_date->YYYY = prev->YYYY;
            next_date->day = day;
        }else if( ( prev->DD == monthDays[prev->MM] ) && (prev->MM == 12)  )
        {
            next_date->YYYY = prev->YYYY + 1;
            next_date->MM = 1;
            next_date->DD = 1; 
            next_date->day = day;
        }else if ( prev->DD == monthDays[prev->MM] )
        {
            next_date->DD = 1;
            next_date->MM = prev->MM + 1 ;
            next_date->YYYY = prev->YYYY;
            next_date->day = day;
        }else
        {
            next_date->DD = prev->DD +1 ; 
            next_date->MM = prev->MM;
            next_date->YYYY = prev->YYYY;
            next_date->day = day;
        }
    }
    return next_date ; 
}


char week[7][5] = { "Mo" , "Tu" , "We" , "Th" , "Fr" , "Sat" , "Sun" };
char* get_day(int day )
{
    return week[day]; 
}

