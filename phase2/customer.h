#ifndef CUSTOMER_H
#define CUSTOMER_H
#define MAX_NAME_SIZE 1000
#define MAX_CUSTOMER 50
#define CUSTOMER_FILE "customer.txt"
#define CUSTOMER_FILE_OP "customer_op.txt"
#include <stdio.h>
#include "date.h"
#include "bill.h"
#define MAX_BILLS_PERSON 500
struct customer 
{
  int uid__ ; 
  int tt_purchase__;
  char name__ [MAX_NAME_SIZE];
  bill_t customer_bill__[MAX_BILLS_PERSON];

};
typedef struct customer customer_t;

customer_t* __find_customer_UID ( char* name , customer_t* db_customer);
void insert_customer( customer_t* customer , char* name , float amt ,date_t date , bill_t* db_bills,customer_t* db_customer ,int time) ;
void fdisplay_customer(customer_t* customer , FILE *fp);
void all_customers_fdisplay( customer_t* db_customer );
int get_tt_customers();




#endif