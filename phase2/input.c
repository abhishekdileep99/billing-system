#include "date.h"
#include "input.h"
#include "customer.h"
#include "bill.h"
#include<stdio.h> 
#include<windows.h>
#include<stdlib.h>
#include <time.h>
#include <string.h>

#define __MAX_CHAR 99
#define __MIN_BILL 20
#define __MAX_BILL 40
#define __MIN_AMT 100
#define __MAX_AMT 500
#define TUE 1
#define SAT 5
#define SUN 6

void read_input( customer_t *db_customer , bill_t *db_bills )
{
    
    date_t strtdt ; 
    date_t enddt;
    time_t t ; 
    char *names[50];
    char str[100];

    srand( (unsigned) time(&t) );
    input_Customer(names);
    input_range_Date( &strtdt , &enddt );

    date_t* nextDT = &strtdt; 
    date_t* end_dt = &enddt;
    end_dt = next_date(end_dt);
    customer_t *cust_file ; 
    int tt_days = calculate_day(&strtdt , end_dt);
 
      
    for(int i = 0 ; i < tt_days ; i++  )
    {
        if(nextDT->day != TUE)
        {
            
            __gen_day_bill(   str , names  , cust_file , nextDT , db_bills , db_customer);
        }        
       nextDT = next_date(nextDT);
        
    }
    
    time_checker(db_bills);

    time_checker(db_bills);

   
}

void __gen_day_bill(   char *str , char *names[50] ,  customer_t *cust_file , date_t *startdt ,  bill_t* db_bills,customer_t* db_customer)
{
    
    
    int tt_bills_per_day ; 
    int hh = 8;
    int min = 00;
    if(startdt->day == SUN || startdt->day == SAT)
    {
        tt_bills_per_day = random_int( 60 , 80 );
    }else 
    {
        tt_bills_per_day = random_int( __MIN_BILL , __MAX_BILL);
    }
    
    for(int j = 0 ; j < tt_bills_per_day ; j++ )
        {
            // do
            // {
            //     random_name(str , names);
            // }while( !__is_unique( str , unique_arr , nName) );
            
            // unique_arr[nName] = (char *)malloc(sizeof(char)* 9999);
            // strcpy(unique_arr[nName] , str);
            // nName++;
            //printf("%d " , random_time( &hh , &min ) );
             
            random_name( str , names );
            
            int amt = random_int(__MIN_AMT , __MAX_AMT );
            insert_customer( cust_file , str , amt  , *startdt , db_bills , db_customer , random_time( &hh , &min , ( (11*60) / tt_bills_per_day ) )  );
        }
     
}
//Change time input if asked 
int random_time(int *hh , int *min , int inc )
{
    if(*min >= 60 )
        {
            *min = 0;
            *hh = *hh+1;
        }
    int rand_time = random_int(0 , inc - 1);
    int time = (*hh)*(100) + *min + rand_time ;
    *min = *min + inc ; 
    
    int min_ptr = time%100;
    if(min_ptr > 59) 
    {
        min_ptr -= inc; 
        min_ptr += 1 ;

    }

    if( time > 2200 )
        {
            perror("Time bill no genearted : shop closed ");
            exit(EXIT_FAILURE);
        }  
    if( 1300 <= time && time <= 1600 )
        time = random_time(hh , min , inc );
          
        return time ; 
}

int __is_unique(char *str , char *names[50] , int nName )
{  
    for(int i =  0 ; i < nName ; i++ )
    {
        if( strncmp( str , names[i] , 9999) == 0 )
            return 0; 
    }
    return 1; 
}


int time_checker( bill_t* db_bills )
{
     int tt_bills = get_tt_bills();
    for(int i = 0 ; i < tt_bills ; i++ )
    {
        int time = db_bills[i].time_;
        if ( time %100 > 59 )
        {
            db_bills[i].time_ = db_bills[i].time_ + 40 ;
        }
        time = db_bills[i].time_;
        if (time > 2200 )
        {
           db_bills[i].time_ = db_bills[i].time_ -100 ;
        }else if(time >= 1300 && time <= 1600 ){
            db_bills[i].time_ = db_bills[i].time_ + 500 ; 
        }
    }
}

void input_range_Date(date_t *strtdt , date_t *enddt)
{

    FILE *fp = fopen( INPUT_DATE , "r+");
    char* buffer = (char *)malloc(sizeof(buffer) * __MAX_CHAR); 
    if(fp == NULL )
    {
        perror("file not exisits");
        exit(EXIT_FAILURE);
    }
    int MM;
    int DD;
    int YYYY;
    int day; 
    fscanf( fp , "%d-%d-%d %d\n" , &DD ,&MM ,&YYYY , &day);
    day_checker(day);
    insert_date(MM , DD , YYYY , strtdt ,  day );
    fscanf( fp , "%d-%d-%d\n" , &DD ,&MM ,&YYYY);
    insert_date(MM , DD , YYYY , enddt , 0 );
    int diff_day =  getDifference( *strtdt , *enddt );      
    for(int i = 0 ; i < diff_day ; i++ )
    {
         day = (day+1)%7;   
    }
    insert_date(MM , DD , YYYY , enddt , day );
    enddt = next_date(enddt);
    fclose(fp);
    fflush(fp);
}


void input_Customer(char *names[50]) 
{
    FILE *fp = fopen( NAME_FILE , "r");
    char fbuffer[__MAX_CHAR]; 
    char lbuffer[__MAX_CHAR];  
    int i = 0 ; 
    if(fp == NULL )
    {
        perror("file not exisits");
        exit(EXIT_FAILURE);
    }
    while ( fscanf( fp , "%s %s" , fbuffer , lbuffer ) != EOF )
    {
        names[i] = (char *)malloc(sizeof(char)*100);
        strcpy( names[i] , fbuffer );  
        i++;  
    }
    fclose(fp);
}

void random_name(char *str , char *names[50] )
{   
    strcpy(str , names[ rand() % 50 ] );
}


int random_int( int lower , int upper )
{
    return  ( lower +  (rand()% (upper - lower + 1 ))  ) ; 
}


void fdisplay(customer_t *db_customer , bill_t *db_bills  )
{
    all_customers_fdisplay(db_customer);
    all_bills_fdisplay(db_bills);
}
