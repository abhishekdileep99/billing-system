int __find_customer_UID (FILE *fpntr , char* name)
{
    if (!fpntr) 
    {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    struct stat sb;
    
    if (stat(CUSTOMER_FILE, &sb) == -1) 
    {
        perror("stat");
        exit(EXIT_FAILURE);
    }

    char *file_contents = malloc(sb.st_size);
    int prev = 0 ;
    int prev2 = 0 ; 
    while ( fscanf(fpntr, "%[^\n ] ", file_contents) != EOF)
    {
        if(strcmp( file_contents , name ) == 0   ) 
        {
            return prev2;
        }
        prev2 = prev; 
        prev = ftell(fpntr);
    }
    return INT_MAX;
}



void insert_customer( customer_t* customer_pntr , char* name , float amt , date_t date ) 
{
    FILE *fpntr = fopen(CUSTOMER_FILE , "r"); 
    struct stat sb;
    
    if(fpntr == NULL )
    {
        perror("Error File not Exists");
        exit(EXIT_FAILURE);
    }

    int byte_customer = __find_customer_UID(fpntr , name );
    fclose(fpntr);



    if( byte_customer == INT_MAX )
    {
        customer_pntr = (customer_t * )malloc(sizeof(customer_t));
        customer_pntr->uid__ = UID++ ; 
        strcpy(customer_pntr->name__ , name );
        fpntr = fopen(CUSTOMER_FILE , "a+");
        fprintf(fpntr ,  "\n%d %s %d" , byte_customer , name , 0 );
        fclose(fpntr);
    }
    else 
    {   
        char *file_contents = malloc(sb.st_size);
        int uid ; 
        //Retrieving the old customer's User Id
        fpntr = fopen(CUSTOMER_FILE , "r+");
        fseek(fpntr , byte_customer  , SEEK_SET );
        fscanf(fpntr, "%[^\n ] ", file_contents);
        uid  = atoi(  file_contents );
        fclose( fpntr );
        fflush(fpntr);
        //Create New Bill , add UID as customer id and retrieve the new bill 

        bill_t old_cus ;
        create_bill(uid , date , amt , &old_cus  );
        int bill_no = old_cus.bill_no__;

        //Write the Bill into the file 
        fpntr = fopen(CUSTOMER_FILE , "a+");
        fprintf(fpntr ,  "%d %s %d\n" , uid , name , bill_no );
        fclose(fpntr);

    }
}