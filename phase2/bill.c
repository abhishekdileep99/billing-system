#include "bill.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int __BILL_ID = 0 ;
// bill_t __db_bills[MAX_BILLS_TT];

void create_bill(int c_uid , date_t tdate , float bill_amt  , bill_t *bill_cus ,  bill_t* db_bills , int time )
{
    bill_cus->bill_no__ = __BILL_ID++;
    bill_cus->customer_uid__ = c_uid ;
    bill_cus->bill_amt__ = bill_amt;
    bill_cus->bill_date__ = tdate;
    bill_cus->time_ = time;
    db_bills[bill_cus->bill_no__] = *bill_cus;
}

int  get_bill_no(const bill_t *bill )
{
    return bill->bill_no__;
}

void fdisplay_bill(const bill_t *bill , FILE *fp)
{
    fprintf(fp , "\nCustomer ID : %d Bill Amount : %.2f INR Bill ID : %d Date : " , bill->customer_uid__ , bill->bill_amt__ , bill->bill_no__);
    display_date( &bill->bill_date__ , fp); 
    fprintf(fp , " %4d hrs " , bill->time_  );
}

void all_bills_fdisplay( bill_t* db_bills )
{
    FILE *fp = fopen( BILLS_OP , "w+");

    for(int i = 0 ; i < __BILL_ID ; i++ )
    {
        fdisplay_bill(db_bills + i , fp );
    }
    fclose(fp);
}

int get_tt_bills()
{
    return __BILL_ID;
}