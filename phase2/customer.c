#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "customer.h"
#include "bill.h" 
#include "date.h"
#include <sys/stat.h>
#define MAX_LINE_SIZE 400
#define MAX_CUSTOMER 50
// customer_t __db_customer[MAX_CUSTOMER];
int __UID = 0 ; 

customer_t* __find_customer_UID ( char* name , customer_t* db_customer )
{
   for(int i = 0 ; i < __UID ; i++ )
   {
       if(strcmp (name , db_customer[i].name__ ) == 0)
            return ( db_customer + i );
   }
   return NULL;
}


void fdisplay_customer(customer_t* customer , FILE *fp)
{
    fprintf( fp ,  "%d -> %s\n" , customer->uid__ , customer->name__);
}


void insert_customer( customer_t* customer_pntr , char* name , float amt , date_t date ,bill_t* db_bills,customer_t* db_customer , int time ) 
{ 
    if(strcmp(name , "" ) == 0 )
        {
            perror("String name Empty");
            exit(EXIT_FAILURE);
        }
        if ( time %100 > 59 )
        {
            time = time + 40 ;
        }
        
        if (time > 2200 )
        {
           time = time -100 ;
        }else if(time >= 1300 && time <= 1600 ){
            time = time + 500 ; 
        }

  customer_t* uid = __find_customer_UID(name , db_customer);
    if(uid == NULL)
    {
        customer_t *new_cus =  db_customer + __UID; 
        bill_t *bill = (bill_t *)malloc(sizeof(bill_t));
        strcpy ( new_cus->name__ ,  name);
        new_cus->uid__ = __UID;
        new_cus->tt_purchase__ = 0;
        create_bill(__UID , date , amt , bill, db_bills ,  time );
        new_cus->customer_bill__[new_cus->tt_purchase__] = *bill;
        __UID++;
        new_cus->tt_purchase__ += 1;

    }
    else 
    {
        bill_t *bill = (bill_t *)malloc(sizeof(bill_t));
        create_bill(uid->uid__ , date , amt , bill , db_bills , time);
        uid->customer_bill__[uid->tt_purchase__ ] = *bill;
        uid->tt_purchase__ +=1;
    }

}

void all_customers_fdisplay(customer_t* db_customer)
{
    FILE *fp = fopen(CUSTOMER_FILE_OP , "w+");

    for(int i = 0 ; i< __UID ; i++ )
    {
        fprintf(fp , "\n------------------------\n");
        fdisplay_customer(db_customer + i , fp );
        int j = 0 ;
        while ( j < db_customer[i].tt_purchase__ )
        {
            fdisplay_bill( db_customer[i].customer_bill__ + j , fp  ); 
            j++;
        }
        
        fprintf(fp , "\n------------------------\n");
    }
    fclose(fp);
}

int get_tt_customers()
{
    return __UID; 
}