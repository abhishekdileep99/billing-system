#include "bill.h"
#include "customer.h"
#include "date.h"
#ifndef SALES_DAY_H
#define SALES_DAY_H
#define SALES "report.txt"

void tt_sales_per_customer( customer_t *db_customer  );
void tt_sales_per_day(  bill_t *db_bills , date_t* start_dt , date_t* end_dt );
void tt_sales_per_weekday( bill_t *db_bills , date_t* start_dt , date_t* end_dt );
void fdisplay_tt_customer(char* name  , double amt);
void fdisplay_tt_bill(date_t Date  , double amt);
int unique_date( date_t* all_date , date_t date , int nSize  );
int compare( const date_t* lhs , const date_t* rhs  );
void fdisplay_char( char* str , double amt);
void tt_sales_per_hour(  bill_t *db_bills ,date_t *start_dt , date_t* end_dt );
void fdisplay_char_fp( char* str , double amt , char* file_name );
#endif